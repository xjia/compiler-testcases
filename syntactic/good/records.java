record node {
    int data;
    node next;
}
int main(string[] args) {
    node n;
    n = new node;
    n.data = 1;
    n.next = new node;
    n.next.data = 2;
    n.next.next = null;
    return 0;
}