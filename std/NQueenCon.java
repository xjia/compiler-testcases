/*
I'm not sure about the local variable "i" in printrow(), though the answer is right...
by msh
*/

int main() {
	int n;
	for (n = 6; n <= 11; n = n + 1)
		nqueen(n);
}

int nqueen(int n) {
	int i;
	int c;
	int odd;
	
	i = 0;
	c = n;
	odd = mod(n, 2);
	if (mod(n / 2, 3) != 1) {
		printrow(2, c);
		i = 4;
		while (i <= n) {printrow(i, c); i = i + 2;}
		i = 1;
		while (i <= n) {printrow(i, c); i = i + 2;}
	} else {
		n = n - odd;
		printrow(n / 2, c);
		i = n / 2 + 1;
		while (i != n / 2 - 1)  {
			printrow(i + 1, c); i = mod(i + 2, n);
		}
		i = mod(i - 2, n);
		while (i !=	n / 2 - 1) {
			printrow(n - i, c); i = mod(i - 2, n);
		}
		printrow(n - i, c);
		if (odd) printrow(n + 1, c);
	}
	printString("\n");
}


int printrow(int pos, int c) {
	int i;
	for (i = 1; i <= pos - 1; i = i + 1)  printString(" .");
	printString(" O");
	for (i = pos + 1; i <= c; i = i + 1) printString(" .");
	printString("\n");
}

int mod(int x, int y) {
	x = x + y;
	return x - x / y * y;
}