/*
change the type of IntArray,IntArray2 to int[],int[][]
add parameters in printBoard(),genBoard(),fill(),color(), for transferring variables
add return value of color(), for updating the value of color
by msh
*/

int printBoard(int sz, int[][] board) {
	int i, j;
	for (i = 0;  i < sz; i = i + 1) {
		for (j = 0; j < sz; j = j + 1) {
			printInt(board[i][j]);
			printString(" ");
		}
		printLine("");
	}
}

int[][] genBoard(int n, int sx, int sy, int[] p2) {
	int[][] board;
	int color, i;
	
	board = new int[][p2[n]];
	for (i = 0; i < p2[n]; i = i + 1) {
		board[i] = new int[p2[n]];
		fillIntArray(board[i], 0);
	}
	color = 1;
	
	fill(board, n, 0, 0, sx, sy, 0, p2, color);
	return board;
}

int fill(int[][] bd, int n, int x0, int y0, int sx, int sy, int scolor, int[] p2, int color) {
	int subsz, curcolor, lx, ly, rx, ry;
	int i, j;
	
	if (n == 0) bd[sx][sy] = scolor;
	else {
		subsz = p2[n - 1];
		curcolor = color;
		lx = ly = rx = ry = 0;
		color = color + 1;
		for (i = 0; i <= 1; i = i + 1) 
			for (j = 0; j <= 1; j = j + 1) {
				lx = x0 + i * subsz;
				ly = y0 + j * subsz;
				rx = x0 + (i + 1) * subsz;
				ry = y0 + (j + 1) * subsz;
				if (inRect(sx, sy, lx, ly, rx, ry)) {
					color = fill(bd, n - 1, lx, ly, sx, sy, scolor, p2, color);
				}
				else {
					color = fill(bd, n - 1, lx, ly, x0 + subsz - 1 + i, y0 + subsz - 1 + j, curcolor, p2, color);
				}
			}
	}
	return color;
	
}

int inRect(int x, int y, int x1, int y1, int x2, int y2) {
	return (x >= x1) && (x < x2) && (y >= y1) && (y < y2);
}


int main() {
	int N, i;
	int[] p2;
	
	N = 3;
	p2 = new int[N + 1];
	fillIntArray(p2, 0);
	p2[0] = 1;
	for (i = 1; i <= N; i = i + 1) p2[i] = p2[i - 1] * 2;
	printBoard(p2[N], genBoard(N, p2[N]-1, p2[N]-1, p2));
}