//Dai Bo
//Change Global Variable to Function Parameter
//Change Nested Function to Global Function

int main() {
	int N, M, i, j, primeCount, resultCount;
	int[] bool, prime, gps, tmp;
	int[][] result;
	N = 1000;
	M = 168;
	primeCount = 0;
	resultCount = 0;
	tmp = new int[1];
	tmp[0] = 0;
	bool = new int[N + 1];
	prime = new int[M + 1];
	gps = new int[N + 1];
	fillIntArray(bool, 1);
	fillIntArray(prime, 0);
	fillIntArray(gps, 0);
	result = new int[][M + 1];
	for (i = 0; i <= M; i = i + 1)
	{
		result[i] = new int[M + 1];
		fillIntArray(result[i], -1);
	}
	getPrime(N, tmp, bool, gps, prime);
	primeCount = tmp[0];
	for (i = 1; i < primeCount; i = i + 1)
		for (j = i + 1; j <= primeCount; j = j + 1)
			if (result[i][j] == -1) {
				result[i][j] = getResult(N, i, j, bool, gps, prime, result);
				if (result[i][j] > 1) {
					printF(prime[i], prime[j], result[i][j]);
					resultCount = resultCount + 1;
				}
			}
	printLine("Total: " + resultCount);
}

int getPrime(int N, int[] primeCount, int[] bool, int[] gps, int[] prime) {
	int count, i;
	count = 2;
	for (i = 2; i <= N; i = i + 1) {
		if (bool[i] == 1) {
			primeCount[0] = primeCount[0] + 1;
			prime[primeCount[0]] = i;
			gps[i] = primeCount[0];
		}
		while (i * count <= N) {
			bool[i * count] = 0;
			count = count + 1;
		}
		count = 2;
	}
}

int getResult(int N, int k1, int k2, int[] bool, int[] gps, int[] prime, int[][] result) {
	if (result[k1][k2] == -1) 
		if (prime[k2] * 2 - prime[k1] <= N) 
			if (bool[prime[k2] * 2 - prime[k1]]) 
				result[k1][k2] = getResult(N,
								k2, 
								gps[prime[k2] * 2 - prime[k1]],
								bool,
								gps,
								prime,
								result) + 1;
	if (result[k1][k2] == -1) 
		result[k1][k2] = 1;
	return result[k1][k2];
}

int printF(int k1, int k2, int k3) {
	printInt(k1);
	while (k3 > 0) {
		printChar(' ');
		printInt(k2);
		k2 = k2 * 2 - k1;
		k1 = (k1 + k2) / 2;
		k3 = k3 - 1;
	}
	printChar('\n');
}

