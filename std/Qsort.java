//Dai Bo
//Sort 10000...1 to 1...10000

int main() {
	int[] a;
	int n, i;
	a = new int[10100];
	fillIntArray(a, 0);
	n = 10000;
	for (i = 1; i <= n; i = i + 1)
		a[i] = n + 1 - i;
	qsort(1, n, a);
	for (i = 1; i <= n; i = i + 1) {
		printInt(a[i]);
		printChar(' ');
	}
	printChar('\n');
}

int qsort(int l, int r, int[] a) {
	int i, j, x, temp;
	i = l; j = r; 
	x = a[(l + r) / 2];
	temp = 0;
	while (i <= j) {
		while (a[i] < x) i = i + 1;
		while (a[j] > x) j = j - 1;
		if (i <= j) {
			temp = a[i]; a[i] = a[j]; a[j] = temp;
			i = i + 1;
			j = j - 1;
		}
	}
	if (l < j) qsort(l, j, a);
	if (i < r) qsort(i, r, a);
}