/*
change the type arr to int[]
add parameters in printIntA,printIntB,printBigInt,plus, for transferring variables
note: the variable L in printIntA,printIntB,printBigInt is different from that in main
by msh
*/

int plus(int SIZE, int[] a, int[] b, int[] c){
    int add, j;
	add = j = 0;
	while (j < SIZE){
		c[j] = a[j] + b[j] + add;	  
		add = 0;
		if (c[j] > 9)  {
			c[j] = c[j] - 10;
			add = 1;
		}
		j=j+1;
	}
	if (add > 0) {
		c[j] = 1;
		return j;
	}
	else return j - 1;
}

int printIntA(int L, int[] a) {  
    while (L >= 0) {
		printInt(a[L]);
		L=L-1;
	}
	printString("\n");
}

int printIntB(int L, int[] b) {
    while (L >= 0) {
		printInt(b[L]);
		L=L-1;
	}
	printString("\n");
}
  
int printBigInt(int L, int[] c) {
    while (L >= 0) {
		printInt(c[L]);
		L=L-1;
	}
	printString("\n");
}

int main() {
	int SIZE;
	int[] a, b, c;
	int L, i;
  
	SIZE = 15;
	a = new int[SIZE];
	fillIntArray(a, 0);
	b = new int[SIZE];
	fillIntArray(b, 0);
	c = new int[SIZE*2];
	fillIntArray(c, 0);
	L = 0;
	
	for (i = 0; i < SIZE; i = i + 1) {
		if (i < 9) a[i]=i+1;
		else a[i] = i-9;
	}
	printIntA(SIZE-1, a);
	for (i = 0; i < SIZE; i = i + 1) {
		if (i < SIZE / 2) b[i] = 7;
		else b[i] = 3;
	}
	printIntB(SIZE-1, b);
	L = plus(SIZE, a, b, c);
	printBigInt(L, c);
}