/*
add parameters in inline1,inline2, for transferring variables
by msh
*/
int inline1(int A) {
	printInt(A);
	return 1;
}

int inline2(int a, int A) {
	inline1(A);
	a = 1;
	return 1;
}

int main() {
	int a;
	a = 0;
	printInt(a);
	inline2(9, a);
	printInt(a);
	a = 2;
	inline1(a);
	printInt(a);
}