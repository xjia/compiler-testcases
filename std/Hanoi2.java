/*
change the function concat() to "+"
change the function readint() to readInt() (in the comment)
by msh
*/

int hanoi(string a, string b, string c, int n) {
	if (n > 1) {
		hanoi(a, c, b, n-1);
		printString(a + " -> " + c + "\n");
		hanoi(b, a, c, n-1);
	}
	else {
		printString(a + " -> " + c + "\n");
	}
}

int main() {
	int N;
	/*   N = readInt(); */
	N = 12;
	if (N > 0) hanoi("a", "b", "c", N);
}