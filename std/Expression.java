/*
add parameters in calc(),find(), for transferring variables
change the compare function of strings (equal() in Expression.tig) to "=="
change the function ord(s) in Expression.tig where s is a string to ord(s[0])
by msh
*/

int calc(int s, int e, string str) {
	int ret, i, j, first, second;
	string tmp;
	
	ret = 0;
	i = s;
	j = 0;
	first = second = -1;
	tmp = "";
	
	if (s == e) {
      ret = ord(substring(str,i,1)[0])-ord('0');
	}
	else {
		while (i < e) {
			tmp = substring(str,i,1);
			if (tmp == "(") {
				i = find(i, str);
			} else if (tmp == "*" || tmp == "/") {
				first = i;
				i = i + 1;
			} else if (tmp == "+" || tmp == "-") {
				second = i;
				i = i + 1;
			} else {
				i = i + 1;
			}
		}
		if (second >= 0) {
			if (substring(str,second,1) == "+") {
				ret = calc(s,second-1, str)+calc(second+1,e, str);
			}
			else {
				ret = calc(s,second-1, str)-calc(second+1,e, str);
			}
		} else if (first >= 0) {
			if (substring(str,first,1) == "*") 
				ret = calc(s,first-1, str)* calc(first+1,e, str);
			else ret = calc(s,first-1, str)/calc(first+1,e, str);
		} else ret=calc(s+1,e-1, str);
	}
	return ret;
}

int find(int s, string str) {
	int cnt, i;
	
	cnt = 1;
	i = s + 1;
	
	while (cnt > 0) {
		if (substring(str,i,1) == "(") cnt = cnt+1;
		else if (substring(str,i,1) == ")") cnt = cnt-1;	
		i = i+1;
	}
	return i-1;
}


int main() {
	string str;
	str = "";
	str = "1+3+5+7+9*2";
	printInt(calc(0,str.length-1, str));
	printLine("");
	str = "8/4*3-(5+9-4)";
	printInt(calc(0,str.length-1, str));
	printLine("");
	str = "7+8*(3+2*4)";
	printInt(calc(0,str.length-1, str));
	printLine("");
}
