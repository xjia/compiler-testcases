/*
add paremeter and return value of cd(), for updating the value of sum
combine several print() in Hanoi.tig into one printLine()
by msh
*/

int cd(int d,int a,int b,int c, int sum) {
	if (d == 1) {
		printLine("move " + chr(a) + " --> " + chr(c) );
		sum = sum + 1;
	} else {
		sum = cd(d-1,a,c,b, sum);
		printLine("move " + chr(a) + " --> " + chr(c) );
		sum = cd(d-1,b,a,c, sum);
		sum = sum + 1;
	}
	return sum;
}

int main() {
	int a, b, c, d, sum;
	a = 65;
	b = 66;
	c = 67;
	d = 10;
	sum = 0;
	sum = cd(d,a,b,c, sum);
	printInt(sum);
}