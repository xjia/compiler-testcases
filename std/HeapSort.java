/*
	Heap Sort
	By TLP
*/


/*
change the type arr to int[]
add parameters in adjustHeap,makeHeap,heapSort, for transferring variables
by msh
*/

int makeHeap(int[] a, int n) {
	int i, t, j;
	i = (n - 1) / 2;
	t = j = 0;
	while (i >= 0) {
		j = i * 2;
		if (i*2+1<n && a[i*2+1]<a[i*2]) j = i*2+1;
		if (a[i] > a[j]) {
			t = a[i];
			a[i] = a[j];
			a[j] = t;
		}
		i = i - 1;
	}
}

int adjustHeap(int n, int[] a) {
	int i, j, t;
	i = j = t = 0;
	while (i * 2 < n) {
		j = i*2;
		if (i*2+1<n && a[i*2+1] < a[i*2]) j = i*2+1;
		if (a[i] > a[j]) {
			t = a[i];
			a[i] = a[j];
			a[j] = t;
			i = j;
		}
		else break;
	}
}

int heapSort(int[] a, int n) {
	int t, k;
	t = 0;
	for (k = 0; k < n; k = k + 1) {
		t = a[0];
		a[0] = a[n-k-1];
		a[n-k-1] = t;
		adjustHeap(n-k-1, a);
	}
}

int main() {
	int n, i;
	int[] a;
	
	a = new int[10000];
	fillIntArray(a, 0);
	n = 10000;
	for (i = 0; i < n; i = i + 1) a[i] = i;
	makeHeap(a, n);
	heapSort(a, n);
	for (i = n / 2; i <= n / 2 + 1; i = i + 1) {
		printInt(a[i]);
		printString(" ");
	}
}

/* result:
99 98 ... 0
*/
			
			
